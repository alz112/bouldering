# Bouldering

## Commands

- npm run scrape
- npm run start

## Routes

- /boulders/list

## Examples:

```bash
curl 'http://localhost:3000/boulders/list?near=48.38,2.53,100000&rating=>4&type=~wall&skip=1&limit=3'
```
