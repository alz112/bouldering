export const BASE_URL = 'https://bleau.info';
export const ASSET_CACHE = __dirname + '/../assets/cache';
export const AREAS_JSON = __dirname + '/../assets/areas.json';

export const DB_NAME = 'bouldering';
export const COLLECTION_BOULDERS = 'boulders';
