import { DB_NAME } from './constants';
import {
  Collection,
  Db,
  IndexOptions,
  MongoClient,
  MongoClientOptions,
} from 'mongodb';

export class MongoDB {
  private db: Db;
  private client: MongoClient;

  public constructor(
    private connectionString: string,
    private databaseName: string = DB_NAME,
    db?: Db,
    client?: MongoClient
  ) {
    this.db = db;
    this.client = client;
  }

  public async connect() {
    const options: MongoClientOptions = {
      sslValidate: true,
      keepAlive: true,
      keepAliveInitialDelay: 1000,
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 1000,
      connectTimeoutMS: 1000,
      useNewUrlParser: true,
    };

    this.client = await MongoClient.connect(this.connectionString, options);
    this.db = await this.client.db(this.databaseName);
  }

  public async getCollection(collectionName: string): Promise<Collection> {
    return this.ensureCollection(collectionName);
  }

  public async dropCollection(collectionName: string): Promise<void> {
    await this.db.dropCollection(collectionName);
  }

  public async ensureIndex(
    collectionName: string,
    fieldOrSpec: any,
    options?: IndexOptions
  ): Promise<string> {
    const collection = await this.ensureCollection(collectionName);
    const ensured = await collection.createIndex(fieldOrSpec, options);
    return ensured;
  }

  private async ensureCollection(collectionName: string): Promise<Collection> {
    const collections = await this.db
      .listCollections({ name: collectionName })
      .toArray();

    const collection = collections.length
      ? await this.db.collection(collectionName)
      : await this.createCollection(collectionName);

    return collection;
  }

  private async createCollection(collectionName: string): Promise<Collection> {
    const collection = await this.db.createCollection(collectionName);
    return collection;
  }
}
