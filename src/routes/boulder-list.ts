import { RequestHandler, Request, Response } from 'express';
import { extractDefaultParams } from './mongoquery';
import { Collection } from 'mongodb';
import { countPipeline, itemPipeline } from './pipelines';

export function listHandler(boulders: Collection): RequestHandler {
  return async (req: Request, res: Response) => {
    try {
      const { match, skip, limit, geo } = extractDefaultParams(req);
      console.log('Listing boulders w/params', match, skip, limit, geo);

      let items = [];
      let count = 0;

      const countAgPipe = countPipeline(match, geo);
      const countCursor = await boulders.aggregate(countAgPipe);
      const countResult = await countCursor.toArray();
      count = countResult.length > 0 ? countResult[0].count : 0;

      if (count > 0) {
        const itemAgPipe = itemPipeline(match, skip, limit, geo);
        const itemCursor = await boulders.aggregate(itemAgPipe);
        items = await itemCursor.toArray();
      }

      return res
        .json({
          boulders: items,
          total: count,
        })
        .status(200);
    } catch (error) {
      return res
        .json({
          error,
        })
        .status(424);
    }
  };
}
