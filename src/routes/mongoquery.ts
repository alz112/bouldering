import { Request } from 'express';

// tslint:disable-next-line:no-var-requires
const MongoQS = require('mongo-querystring');

export const ORDER_ASC = 'ASC';
export const ORDER_DESC = 'DESC';

export const LIMIT_DEFAULT = 100;
export const LIMIT_MAX = 50000;

export function extractDefaultParams(request: Request) {
  const params: any = request.query;

  // tslint:disable:prefer-const
  let { skip, offset, sort, sortBy, order, orderBy, limit, ...query } = params;

  skip = params.skip
    ? parseInt(params.skip, 10)
    : params.offset
    ? parseInt(params.offset, 10)
    : 0;

  limit =
    params.limit !== undefined ? parseInt(params.limit, 10) : LIMIT_DEFAULT;

  sort =
    sort && [ORDER_ASC, ORDER_DESC].includes(sort.toUpperCase())
      ? sort.toUpperCase()
      : ORDER_DESC;

  const qs = new MongoQS({
    custom: { bbox: 'geo', near: 'geo' },
  });
  const match: any = qs.parse(query);

  // We need to ensure geo is excluded from the match and converted to $geoNear aggregation stage
  const geo = match.geo ? to$GeoNear(match.geo.$near) : null;
  delete match.geo;

  return {
    match,
    limit,
    skip,
    sort: sort === ORDER_ASC ? 1 : -1,
    sortBy,
    geo,
  };
}

interface INearQueryOperator {
  $geometry: {
    coordinates: number[];
    type: string;
  };
  $maxDistance: number;
}

function to$GeoNear($near: INearQueryOperator) {
  return {
    near: {
      type: $near.$geometry.type,
      coordinates: $near.$geometry.coordinates,
    },
    distanceField: 'dist.calculated',
    maxDistance: $near.$maxDistance,
    includeLocs: 'dist.location',
    limit: LIMIT_MAX,
  };
}
