export function itemPipeline(
  match: object,
  skip: number,
  limit: number,
  geo: object = null
): object[] {
  let pipeline: object[] = geo ? [{ $geoNear: geo }] : [];

  pipeline = pipeline.concat([
    { $match: match },
    { $skip: skip },
    { $limit: limit },
  ]);

  return pipeline;
}

export function countPipeline(match: object, geo: object = null) {
  let pipeline: object[] = geo ? [{ $geoNear: geo }] : [];

  pipeline = pipeline.concat([
    { $match: match },
    {
      $group: {
        _id: 'null',
        count: { $sum: 1 },
      },
    },
  ]);

  return pipeline;
}
