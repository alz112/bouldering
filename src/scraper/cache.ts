import { ASSET_CACHE } from '../constants';
import { existsSync, writeFileSync, readFileSync, mkdirSync } from 'fs';
import axios from 'axios';

let cacheDirEnsured = false;
export async function fetchHTML(url: string, useCache: boolean = true) {
  if (!cacheDirEnsured) {
    if (!existsSync(ASSET_CACHE)) {
      mkdirSync(ASSET_CACHE);
    }
    cacheDirEnsured = true;
  }

  const location = `${ASSET_CACHE}/${encodeURIComponent(url)}.html`;

  if (!useCache || !existsSync(location)) {
    const response = await axios.get(url);
    if (response.status !== 200) {
      throw new Error(
        `Could not fetch HTML from ${url}, error: ${response.data}`
      );
    }
    writeFileSync(location, response.data);
  }

  return readFileSync(location).toString();
}
