import { parse } from 'node-html-parser';
import { transform } from './transformer';
import { MongoDB } from '../mongodb';
import { Collection } from 'mongodb';
import { COLLECTION_BOULDERS, AREAS_JSON, BASE_URL } from '../constants';
import get from 'lodash.get';
import { sanitizePageUrls, sanitizeBoulderUrls } from './util';
import { fetchHTML } from './cache';

require('dotenv').config();

let mongo: MongoDB;
let bouldersDB: Collection;

// tslint:disable-next-line: no-floating-promises
(async () => {
  console.log(`Connecting to DB`);
  try {
    const mongoDbConn: string = `${process.env.MONGO_CONNECTIONSTRING}`;
    mongo = new MongoDB(mongoDbConn);
    await mongo.connect();
    bouldersDB = await mongo.getCollection(COLLECTION_BOULDERS);
  } catch (error) {
    console.log(`Could not connect to DB, error: ${error.message}`);
    process.exit(1);
    return;
  }

  // Used for geo lookup, unfortunately only geo coords by area
  const areaJSON = await import(AREAS_JSON);
  const areaJSONKeys = Object.keys(areaJSON);
  const areaDict = {};
  for (const areaJSONKey of areaJSONKeys) {
    const area = areaJSON[areaJSONKey];
    areaDict[area.url] = area;
  }

  console.log(`Scraping bleau.info...`);
  const areasHTML = await fetchHTML(`${BASE_URL}/areas_by_region`);

  // Fetch areas
  const root = (parse(areasHTML) as unknown) as HTMLElement;
  let linkElements = Array.from(root.querySelectorAll('a'));
  let areas = linkElements.map(el => (el.attributes as any).href);
  areas = sanitizePageUrls(areas);

  // Fetch all boulder urls per area
  const boulderUrls = {};
  for (const area of areas) {
    try {
      const areaHTML = await fetchHTML(`${BASE_URL}${area}?locale=en`);
      const areaRoot = (parse(areaHTML) as unknown) as HTMLElement;
      linkElements = Array.from(areaRoot.querySelectorAll('a'));
      let urls = linkElements.map(el => (el.attributes as any).href);
      urls = sanitizePageUrls(urls);
      urls = sanitizeBoulderUrls(urls, area);
      boulderUrls[area] = urls;
    } catch (error) {
      console.log(`Could not fetch boulders for area, error: ${error.message}`);
    }
  }

  for (const area of Object.keys(boulderUrls)) {
    console.log(`Fetching boulders for area ${area}...`);
    const boulders = [];
    for (const boulderUrl of boulderUrls[area]) {
      const boulderHTML = await fetchHTML(`${BASE_URL}${boulderUrl}?locale=en`);
      const boulder = transform(
        (parse(boulderHTML) as unknown) as HTMLElement,
        boulderUrl
      );

      if (boulder) {
        if (areaDict[boulder.area]) {
          boulder.geo = {
            lat: parseFloat(get(areaDict[boulder.area], 'latitude')),
            lng: parseFloat(get(areaDict[boulder.area], 'longitude')),
          };
        }
        boulders.push(boulder);
      }
    }

    console.log(`Inserting ${boulders.length} boulders`);
    for (const boulder of boulders) {
      await bouldersDB.replaceOne({ id: boulder.id }, boulder, {
        upsert: true,
      });
    }
  }

  console.log('Done');
  process.exit(0);
})();
