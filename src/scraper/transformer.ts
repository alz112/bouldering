import { IBoulder } from '../types';
import get from 'lodash.get';

export function transform(element: Element, url: string): IBoulder | null {
  const result = /\/(?<area>.*)\/(?<id>\d+)\.html/.exec(url);
  const title = element.querySelector('.btitle h3');
  if (!title || !result) {
    return null;
  }

  const details = element.querySelectorAll('.bdetails .bopins');
  const rating = extractRating(Array.from(details));

  const type = element.querySelector('.btype');
  const description = element.querySelector('.bdesc');

  const boulder: IBoulder = {
    id: parseInt(get(result, 'groups.id', -1), 10),
    title: get(title, 'childNodes[0].rawText', '').replace(/\s/g, ''),
    area: get(result, 'groups.area'),
    type: get(type, 'structuredText'),
    description: get(description, 'structuredText'),
    grade: get(title.querySelector('em'), 'structuredText'),
    rating,
  };

  return boulder.id !== -1 ? boulder : null;
}

function extractRating(elements: Element[]) {
  let rating = -1;
  let result;
  for (const d of elements) {
    if (rating !== -1) break;
    const dText = get(d, 'structuredText', '');
    const dParts = dText.split('\n');
    for (const dPart of dParts) {
      result = /(?<rating>[0-9]*\.?[0-9]+)\ Stars/.exec(dPart);
      const ratingResult = get(result, 'groups.rating', false);
      if (ratingResult) {
        rating = parseFloat(ratingResult);
        break;
      }
    }
  }
  return rating;
}
