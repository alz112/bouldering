export function sanitizePageUrls(urls: string[]) {
  const blacklist = [
    '/',
    '/map',
    '/areas',
    '/login',
    '/areas_by_region',
    '/advanced-search',
  ];

  return urls
    .filter(el => !blacklist.includes(el))
    .filter(el => !el.startsWith('?'))
    .filter(el => !el.startsWith('/toggle_'));
}

export function sanitizeBoulderUrls(urls: string[], area: string) {
  return urls
    .filter(el => el.startsWith(area))
    .filter(el => el.endsWith('.html'));
}