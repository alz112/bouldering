import express from 'express';
import { listHandler } from './routes/boulder-list';
import { MongoDB } from './mongodb';
import { COLLECTION_BOULDERS } from './constants';
import { Collection } from 'mongodb';

require('dotenv').config();

let mongo: MongoDB;
let bouldersDB: Collection;

async function boot(port: number) {
  await initDB();

  const app = express();
  app.listen(port);
  app.get('/boulders/list', listHandler(bouldersDB));
  console.log(`Server listening on port ${port}`);
}

try {
  // tslint:disable-next-line: no-floating-promises
  boot(parseInt(process.env.SERVER_PORT, 10));
} catch (error) {
  console.error(
    `Could not boot express app on port ${process.env.SERVER_PORT}`
  );
  process.exit(1);
}

async function initDB() {
  console.log(`Initializing DB`);
  try {
    const mongoDbConn: string = `${process.env.MONGO_CONNECTIONSTRING}`;
    mongo = new MongoDB(mongoDbConn);
    await mongo.connect();
    bouldersDB = await mongo.getCollection(COLLECTION_BOULDERS);
    await mongo.ensureIndex(COLLECTION_BOULDERS, {
      geo: '2dsphere',
    });
  } catch (error) {
    console.log(`Could not connect to DB, error: ${error.message}`);
    process.exit(1);
    return;
  }
}
