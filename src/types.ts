export interface IBoulder {
  id: number;
  title: string;
  area: string;
  type: string;
  description: string;
  grade: string;
  rating: number;
  geo?: {
    lat: number;
    lng: number;
  };
}
